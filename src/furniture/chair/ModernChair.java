package furniture.chair;

public class ModernChair implements Chair {
    private final String variant;
    private final String fabric;


    public ModernChair() {
        this.variant = "ArtDeco";
        this.fabric = "Linen";
    }

    public String getVariant() {
        return variant;
    }

    public String getfabric() {
        return fabric;
    }

    public int getNoOfLegs() {
        return 0;
    }

    @Override
    public void getChair() {
        System.out.println("Getting the Modern chair...");
    }

    @Override
    public boolean hasLegs() {
        return false;
    }
}
