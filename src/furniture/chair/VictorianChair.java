package furniture.chair;

public class VictorianChair implements Chair {
    private final String variant;
    private final int noOfLegs;
    private final String material;


    public VictorianChair() {
        this.variant = "Victorian";
        this.material = "Leather";
        this.noOfLegs = 5;
    }

    public String getVariant() {
        return variant;
    }

    public String getMaterial() {
        return material;
    }

    public int getNoOfLegs() {
        return noOfLegs;
    }
    @Override
    public void getChair() {
        System.out.println("Getting the Victorian chair...");
    }

    @Override
    public boolean hasLegs() {
        return true;
    }
}
