package furniture.chair;

public interface Chair {
    void getChair();
    boolean hasLegs();
}

