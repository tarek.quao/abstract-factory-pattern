package furniture.table;

public class ModernTable implements Table {

    private String variant;

    public ModernTable() {
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    @Override
    public void getTable() {
        System.out.println("Getting the Modern table...");
    }

    @Override
    public boolean hasLegs() {
        return true;
    }
}
