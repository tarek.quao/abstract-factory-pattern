package furniture.table;

public class VictorianTable implements Table {
    private String variant;

    public VictorianTable() {
        this.variant = "Victorian";
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    @Override
    public void getTable() {
        System.out.println("Getting the Victorian table...");

    }

    @Override
    public boolean hasLegs() {
        return true;
    }
}
