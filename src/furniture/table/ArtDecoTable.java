package furniture.table;

public class ArtDecoTable implements Table {
    private final String variant;

    public ArtDecoTable() {
        variant = "furniture.ArtDeco";
    }

    public String getVariant() {
        return variant;
    }


    @Override
    public void getTable() {
        System.out.println("Getting the ArtDeco table...");

    }

    @Override
    public boolean hasLegs() {
        return true;
    }
}
