package furniture.sofa;

public class ModernSofa implements Sofa {

    private final String variant;
    private final String fabric;

    public String getfabric() {
        return fabric;
    }

    public ModernSofa() {
        this.variant = "Modern";
        this.fabric= "Linen";
    }

    public int getNoOfLegs() {
        return 0;
    }

    public String getVariant() {
        return variant;
    }

    @Override
    public void getSofa() {
        System.out.println("Getting the Modern table...");
    }

    @Override
    public boolean hasLegs() {
        return false;
    }
}
