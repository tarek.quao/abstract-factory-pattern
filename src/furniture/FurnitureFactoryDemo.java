package furniture;

import furniture.chair.ArtDecoChair;
import furniture.chair.Chair;
import furniture.chair.ModernChair;
import furniture.factories.FurnitureFactory;
import furniture.sofa.Sofa;

public class FurnitureFactoryDemo {


    public static void main(String[] args) {
        FurnitureFactory ArtDecoFurnitureFactory = FactoryProducer.getFactory("ArtDeco");
        FurnitureFactory ModernFurnitureFactory = FactoryProducer.getFactory("Modern");
        FurnitureFactory VictorianFurnitureFactory = FactoryProducer.getFactory("Victorian");

        Chair artChair = ArtDecoFurnitureFactory.createChair();

        Chair modernChair = ModernFurnitureFactory.createChair();

        System.out.println(artChair.hasLegs());
        System.out.println(modernChair.hasLegs());



        //ArtDecoChair JsChair = ArtDecoFurnitureFactory.createChair();


        //System.out.println(JsChair.hasLegs());

        //ModernChair myModernChair = ModernFurnitureFactory.createChair();



        //Sofa ms = ModernFurnitureFactory.createSofa();

    }
}
