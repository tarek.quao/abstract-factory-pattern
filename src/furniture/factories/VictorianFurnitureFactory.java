package furniture.factories;

import furniture.chair.Chair;
import furniture.chair.VictorianChair;
import furniture.sofa.Sofa;
import furniture.sofa.VictorianSofa;
import furniture.table.Table;
import furniture.table.VictorianTable;

public class VictorianFurnitureFactory implements FurnitureFactory {

    @Override
    public Chair createChair() {
        return new VictorianChair();
    }

    @Override
    public Sofa createSofa() {
        return new VictorianSofa();
    }

    @Override
    public Table createTable() {
        return new VictorianTable();
    }
}
