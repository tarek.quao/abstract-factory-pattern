package furniture.factories;

import furniture.chair.Chair;
import furniture.sofa.Sofa;
import furniture.table.Table;

public interface FurnitureFactory {
    public Chair createChair();
    public Sofa createSofa();
    public Table createTable();
}


