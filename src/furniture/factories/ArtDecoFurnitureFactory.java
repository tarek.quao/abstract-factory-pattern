package furniture.factories;

import furniture.chair.ArtDecoChair;
import furniture.sofa.ArtDecoSofa;
import furniture.table.ArtDecoTable;

public class ArtDecoFurnitureFactory implements FurnitureFactory {
    @Override
    public ArtDecoChair createChair() {
        return new ArtDecoChair();
    }

    @Override
    public ArtDecoSofa createSofa() {
        return new ArtDecoSofa();
    }

    @Override
    public ArtDecoTable createTable() {
        return new ArtDecoTable();
    }
}
