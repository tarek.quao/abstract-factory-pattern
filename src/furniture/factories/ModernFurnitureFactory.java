package furniture.factories;

import furniture.chair.ModernChair;
import furniture.sofa.ModernSofa;
import furniture.table.ModernTable;

public class ModernFurnitureFactory implements FurnitureFactory {

    @Override
    public ModernChair createChair() {
        return new ModernChair();
    }

    @Override
    public ModernSofa createSofa() {
        return new ModernSofa();
    }

    @Override
    public ModernTable createTable() {
        return new ModernTable();
    }
}
