package furniture;

import furniture.factories.FurnitureFactory;
import furniture.factories.ArtDecoFurnitureFactory;
import furniture.factories.ModernFurnitureFactory;
import furniture.factories.VictorianFurnitureFactory;

public class FactoryProducer {
    public static FurnitureFactory getFactory(String choice){
        if (choice.equalsIgnoreCase("ArtDeco"))
            return new ArtDecoFurnitureFactory();
        else if (choice.equalsIgnoreCase("Modern"))
            return new ModernFurnitureFactory();
        else if (choice.equalsIgnoreCase("Victorian"))
            return new VictorianFurnitureFactory();
        return null;
    }
}
